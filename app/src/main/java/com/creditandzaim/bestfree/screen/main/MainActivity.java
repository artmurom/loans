package com.creditandzaim.bestfree.screen.main;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.creditandzaim.bestfree.App;
import com.creditandzaim.bestfree.R;
import com.creditandzaim.bestfree.content.Loan;
import com.creditandzaim.bestfree.repository.LoansRepository;
import com.willy.ratingbar.ScaleRatingBar;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements MainView {

    private static final String TAG = "MainActivity";

    @Inject
    LoansRepository mRepository;

    private LinearLayout mLlList;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mLlList = findViewById(R.id.llList);

        mHandler = new Handler();

        App.getAppComponent().injectMainActivity(this);

        MainPresenter presenter = new MainPresenter(this, mRepository);
        presenter.init();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void initUI(List<Loan> loanList) {
        mLlList.removeAllViewsInLayout();

        int last = loanList.size() - 1;

        for (int i = 0; i < loanList.size(); i++) {
            Loan loan = loanList.get(i);

            @SuppressLint("InflateParams") View view = LayoutInflater.from(this).inflate(R.layout.item_loan, null, false);

            ImageView ivLogo = view.findViewById(R.id.ivLogo);
            ScaleRatingBar scaleRatingBar = view.findViewById(R.id.simpleRatingBar);
            TextView tvDesc = view.findViewById(R.id.tvDesc);
            Button btnGetMoney = view.findViewById(R.id.btnGetMoney);

            String imagePath;

            try {
                imagePath = "image/" + loan.getLogo();
                InputStream ims = getAssets().open(imagePath);
                Drawable d = Drawable.createFromStream(ims, null);
                ivLogo.setImageDrawable(d);
                ims.close();
            } catch (IOException ex) {
                Log.e(TAG, " error load assets");
            }

            float rating = ((float) loan.getRating()) / 20;

            mHandler.postDelayed(() -> scaleRatingBar.setRating(rating), 10);

            tvDesc.setText(String.format("%s\n%s\n%s", loan.getDesc1(), loan.getDesc2(), loan.getDesc3()));

            btnGetMoney.setOnClickListener(v -> {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(loan.getLink()));
                startActivity(browserIntent);
            });

            if (i == last) {
                view.findViewById(R.id.viewLine).setVisibility(View.GONE);
            }

            mLlList.addView(view);
        }


    }

}
