package com.creditandzaim.bestfree.screen.main;


import com.creditandzaim.bestfree.content.Loan;

import java.util.List;

interface MainView {
    void initUI(List<Loan> loanList);
}
