package com.creditandzaim.bestfree.screen.main;

import com.creditandzaim.bestfree.content.Loan;
import com.creditandzaim.bestfree.repository.LoansRepository;

import java.util.List;


public class MainPresenter {

    private LoansRepository mRepository;
    private MainView mView;

    MainPresenter(MainView view, LoansRepository repository) {
        mView = view;
        mRepository = repository;
    }

    public void init() {
        List<Loan> loanList = mRepository.getLoanList();
        mView.initUI(loanList);
    }
}
