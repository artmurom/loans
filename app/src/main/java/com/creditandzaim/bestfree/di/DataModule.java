package com.creditandzaim.bestfree.di;

import android.content.Context;

import javax.inject.Singleton;

import com.creditandzaim.bestfree.db.DataBaseHelper;
import com.creditandzaim.bestfree.repository.LoansRepository;
import dagger.Module;
import dagger.Provides;

@Module
public class DataModule {

    private Context mContext;

    public DataModule(Context context) {
        mContext = context;
    }

    @Provides
    @Singleton
    DataBaseHelper provideDataBaseHelper(Context context) {
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        dataBaseHelper.upgradeDB();
        return dataBaseHelper;
    }

    @Provides
    @Singleton
    LoansRepository provideLoansRepository(DataBaseHelper dataBaseHelper) {
        return new LoansRepository(dataBaseHelper);
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mContext;
    }

}
