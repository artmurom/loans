package com.creditandzaim.bestfree.di;

import javax.inject.Singleton;

import com.creditandzaim.bestfree.repository.LoansRepository;
import com.creditandzaim.bestfree.screen.main.MainActivity;

import dagger.Component;



@Singleton
@Component(modules = DataModule.class)
public interface AppComponent {
    void injectMainActivity(MainActivity mainActivity);
}
