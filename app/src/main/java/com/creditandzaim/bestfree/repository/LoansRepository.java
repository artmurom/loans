package com.creditandzaim.bestfree.repository;


import android.annotation.SuppressLint;
import android.database.Cursor;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.creditandzaim.bestfree.content.Loan;
import com.creditandzaim.bestfree.db.DataBaseHelper;

public class LoansRepository {

    private static final String TABLE_NAME = "loans";
    private static final String ID_COLUMN = "_id";
    private static final String NAME_COLUMN = "name";
    private static final String LOGO_COLUMN = "logo";
    private static final String DESC_1_COLUMN = "desc_1";
    private static final String DESC_2_COLUMN = "desc_2";
    private static final String DESC_3_COLUMN = "desc_3";
    private static final String LINK_COLUMN = "link";
    private static final String RATING_COLUMN = "rating";

    private static final String DEFAULT_LINK = "http://zcredit00.pro/go.php?articles=3gffduw124x9gm9oj1he&r=%s";

    private DataBaseHelper mDataBaseHelper;

    public LoansRepository(DataBaseHelper dataBaseHelper) {
        mDataBaseHelper = dataBaseHelper;
    }

    public List<Loan> getLoanList() {
        try {
            mDataBaseHelper.openDataBase();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        @SuppressLint("DefaultLocale") String query = String.format("SELECT * FROM %s", TABLE_NAME);

        Cursor cursor = mDataBaseHelper.cursorBack(query);

        List<Loan> loanList = new ArrayList<>();

        while (cursor.moveToNext()) {
            String name = cursor.getString(cursor.getColumnIndex(NAME_COLUMN));
            String logo = cursor.getString(cursor.getColumnIndex(LOGO_COLUMN));
            String desc1 = cursor.getString(cursor.getColumnIndex(DESC_1_COLUMN));
            String desc2 = cursor.getString(cursor.getColumnIndex(DESC_2_COLUMN));
            String desc3 = cursor.getString(cursor.getColumnIndex(DESC_3_COLUMN));
            String linkParametr = cursor.getString(cursor.getColumnIndex(LINK_COLUMN));
            int rating = cursor.getInt(cursor.getColumnIndex(RATING_COLUMN));

            String link = String.format(DEFAULT_LINK, linkParametr);

            Loan loan = new Loan(name, logo, desc1, desc2, desc3, link, rating);

            loanList.add(loan);
        }

        cursor.close();

        mDataBaseHelper.closeDB();
        return loanList;
    }
}
