package com.creditandzaim.bestfree;

import android.app.Application;
import android.support.annotation.NonNull;

import com.creditandzaim.bestfree.di.AppComponent;
import com.creditandzaim.bestfree.di.DaggerAppComponent;
import com.creditandzaim.bestfree.di.DataModule;
import com.yandex.metrica.YandexMetrica;
import com.yandex.metrica.YandexMetricaConfig;


public class App extends Application {


    private static AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        YandexMetricaConfig.Builder configBuilder = YandexMetricaConfig.newConfigBuilder(BuildConfig.APPMETRICA_KEY);
        YandexMetrica.activate(getApplicationContext(), configBuilder.build());
        YandexMetrica.enableActivityAutoTracking(this);

        initDi();
    }

    private void initDi() {
        mAppComponent = DaggerAppComponent.builder()
                .dataModule(new DataModule(this))
                .build();
    }


    @NonNull
    public static AppComponent getAppComponent() {
        return mAppComponent;
    }

}


