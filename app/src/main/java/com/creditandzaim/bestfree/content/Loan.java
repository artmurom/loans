package com.creditandzaim.bestfree.content;

import java.io.Serializable;

public class Loan implements Serializable {

    private final String mName;
    private final String mLogo;
    private final String mDesc1;
    private final String mDesc2;
    private final String mDesc3;
    private final String mLink;
    private final int mRating;

    public Loan(String name, String logo, String desc1, String desc2, String desc3, String link, int rating) {

        mName = name;
        mLogo = logo;
        mDesc1 = desc1;
        mDesc2 = desc2;
        mDesc3 = desc3;
        mLink = link;
        mRating = rating;
    }

    public String getName() {
        return mName;
    }

    public String getLogo() {
        return mLogo;
    }

    public String getDesc1() {
        return mDesc1;
    }

    public String getDesc2() {
        return mDesc2;
    }

    public String getDesc3() {
        return mDesc3;
    }

    public String getLink() {
        return mLink;
    }

    public int getRating() {
        return mRating;
    }
}
